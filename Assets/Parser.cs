﻿using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Diagnostics;
using System.IO;
using UnityEngine;
using System;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

public class Parser : MonoBehaviour
{
    public int chunkSize;
    public int rThreshold;
    public int gThreshold;
    public int bThreshold;
    public string tifFolder;
    public int numChannels;
    public int numStacks;
    public GameObject voxel;
    public GameObject holder;
    public Material[] channelMats;
    public GameObject set;

    private GameObject[] stacks;
    private string[][] tifFiles;
    private byte[][][][][] sequence;
    private int wi, he;

    // Use this for initialization
    void Start()
    {
        if (!tifFolder.EndsWith("/"))
            tifFolder += "/";

        stacks = new GameObject[numStacks];
        for (int i = 0; i < numStacks; i++)
        {
            stacks[i] = new GameObject();
            stacks[i].name = "Stack" + i;
            stacks[i].transform.parent = set.transform;
        }

        tifFiles = new string[numChannels][];
        for (int i = 0; i < numChannels; i++)
        {
            tifFiles[i] = GetFiles(i);
        }

        //StartCoroutine(ParsingRoutine());
        StartCoroutine(LowLevelParsingRoutine2());
    }

    private IEnumerator ParsingRoutine()
    {
        Stopwatch st = new Stopwatch();
        //for (int m = 0; m < tifFiles[0].Length; m++)
        for (int m = 0; m < 1; m++)
        {
            st.Start();
            for (int c = 0; c < numChannels; c++)
            {
                int count = 0;
                string file = tifFiles[c][m];
                List<MeshFilter> voxels = new List<MeshFilter>();
                GameObject[] holders;
                GameObject padre = new GameObject();

                Tiffer t = new Tiffer();
                System.Drawing.Color pixel;
                List<Image> l = t.GetAllPages(file);

                for (int k = 0; k < l.Count; k++)
                {
                    for (int i = 0; i < l[k].Width; i++)
                    {
                        for (int j = 0; j < l[k].Height; j++)
                        {
                            pixel = ((Bitmap)l[k]).GetPixel(i, j);
                            if (pixel.R > rThreshold || pixel.G > gThreshold || pixel.B > bThreshold)
                            {
                                GameObject go = (GameObject)Instantiate(voxel, new Vector3(1f * i / 10f, (k * .1f), 1f * j / 10f), Quaternion.identity);
                                go.transform.parent = padre.transform;
                                voxels.Add(go.GetComponent<MeshFilter>());
                                count++;
                            }
                        }
                    }
                }

                int sets = Mathf.FloorToInt(count / chunkSize);
                holders = new GameObject[sets];
                for (int i = 0; i < sets; i++)
                {
                    holders[i] = Instantiate(holder, Vector3.zero, Quaternion.identity);
                    holders[i].name = c + "-Holder" + i;
                    CombineInstance[] combine = new CombineInstance[chunkSize];
                    for (int j = 0; j < chunkSize; j++)
                    {
                        if (j > count)
                            break;
                        combine[j].mesh = voxels[chunkSize * i + j].sharedMesh;
                        combine[j].transform = voxels[chunkSize * i + j].transform.localToWorldMatrix;
                    }
                    holders[i].GetComponent<MeshFilter>().mesh.CombineMeshes(combine);
                    holders[i].transform.parent = stacks[m].transform;
                    holders[i].GetComponent<Renderer>().material = channelMats[c];
                }
                voxels = null;
                Destroy(padre);
                System.GC.Collect();

                print("channel " + c + " processed - stack " + m);
                yield return null;
            }
            st.Stop();
            TimeSpan ts = st.Elapsed;

            // Format and display the TimeSpan value.
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                ts.Hours, ts.Minutes, ts.Seconds,
                ts.Milliseconds / 10);
            print(elapsedTime);

            stacks[m].SetActive(false);

        }

        set.transform.position = new Vector3(-.351f, .57f, -.39f);
        set.transform.localScale = .027567f * Vector3.one;
    }

    // This one is Marshal with getpixel wrapper
    private IEnumerator LowLevelParsingRoutine()
    {
        Stopwatch st = new Stopwatch();
        //for (int m = 0; m < tifFiles[0].Length; m++)
        for (int m = 0; m < 1; m++)
        {
            st.Start();
            for (int c = 0; c < numChannels; c++)
            {
                int count = 0;
                string file = tifFiles[c][m];
                List<MeshFilter> voxels = new List<MeshFilter>();
                GameObject[] holders;
                GameObject padre = new GameObject();

                Tiffer t = new Tiffer();

                List<Bitmap> l = t.GetAllPagesAsBitmaps(file);

                for (int k = 0; k < l.Count; k++)
                {
                    LockBitmap lockBitmap = new LockBitmap(l[k]);
                    lockBitmap.LockBits();

                    int w = l[k].Width;
                    int h = l[k].Height;

                    for (int i = 0; i < w; i++)
                    {
                        for (int j = 0; j < h; j++)
                        {
                            if (lockBitmap.GetPixel(i,j).GetBrightness() > bThreshold)
                            {
                                GameObject go = (GameObject)Instantiate(voxel, new Vector3(1f * i / 10f, (k * .1f), 1f * j / 10f), Quaternion.identity);
                                go.transform.parent = padre.transform;
                                voxels.Add(go.GetComponent<MeshFilter>());
                                count++;
                            }
                        }
                    }

                    lockBitmap.UnlockBits();
                }

                int sets = Mathf.FloorToInt(count / chunkSize);
                holders = new GameObject[sets];
                for (int i = 0; i < sets; i++)
                {
                    holders[i] = Instantiate(holder, Vector3.zero, Quaternion.identity);
                    holders[i].name = c+"-Holder" + i;
                    CombineInstance[] combine = new CombineInstance[chunkSize];
                    for (int j = 0; j < chunkSize; j++)
                    {
                        if (j > count)
                            break;
                        combine[j].mesh = voxels[chunkSize * i + j].sharedMesh;
                        combine[j].transform = voxels[chunkSize * i + j].transform.localToWorldMatrix;
                    }
                    holders[i].GetComponent<MeshFilter>().mesh.CombineMeshes(combine);
                    holders[i].transform.parent = stacks[m].transform;
                    holders[i].GetComponent<Renderer>().material = channelMats[c];
                }
                voxels = null;
                Destroy(padre);
                System.GC.Collect();

                print("channel " + c + " processed - stack " + m);
                yield return null;
            }
            st.Stop();
            TimeSpan ts = st.Elapsed;

            // Format and display the TimeSpan value.
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                ts.Hours, ts.Minutes, ts.Seconds,
                ts.Milliseconds / 10);
            print(elapsedTime);

            stacks[m].SetActive(false);
        }

        set.transform.position = new Vector3(-.351f, .57f, -.39f);
        set.transform.localScale = .027567f * Vector3.one;
    }

    private IEnumerator LowLevelParsingRoutine2()
    {
        Stopwatch st = new Stopwatch();
        //for (int m = 0; m < tifFiles[0].Length; m++)
        for (int m = 0; m < 1; m++)
        {
            st.Start();
            for (int c = 0; c < numChannels; c++)
            {
                int count = 0;
                string file = tifFiles[c][m];
                List<MeshFilter> voxels = new List<MeshFilter>();
                GameObject[] holders;
                GameObject padre = new GameObject();

                Tiffer t = new Tiffer();

                List<Bitmap> l = t.GetAllPagesAsBitmaps(file);

                for (int k = 0; k < l.Count; k++)
                {
                    BitmapData bitmapData = l[k].LockBits(new Rectangle(0, 0, l[k].Width, l[k].Height), ImageLockMode.ReadWrite, l[k].PixelFormat);

                    int bytesPerPixel = Bitmap.GetPixelFormatSize(l[k].PixelFormat) / 8;
                    int byteCount = bitmapData.Stride * l[k].Height;
                    byte[] pixels = new byte[byteCount];
                    IntPtr ptrFirstPixel = bitmapData.Scan0;
                    Marshal.Copy(ptrFirstPixel, pixels, 0, pixels.Length);
                    int heightInPixels = bitmapData.Height;
                    int widthInBytes = bitmapData.Width * bytesPerPixel;

                    for (int y = 0; y < heightInPixels; y++)
                    {
                        int currentLine = y * bitmapData.Stride;
                        int j = 0;
                        for (int x = 0; x < widthInBytes; x = x + bytesPerPixel)
                        {
                            if (pixels[currentLine+x] > bThreshold || pixels[currentLine + x + 1] > bThreshold || pixels[currentLine + x + 2] > bThreshold)
                            {
                                // TODO this line causes all lag. Check for neighbors. Create 3x3, 5x5 cubes or simply don't draw. Or process at an image pixel level
                                GameObject go = (GameObject)Instantiate(voxel, new Vector3(1f * y / 10f, (k * .1f), 1f * j / 10f), Quaternion.identity);
                                go.transform.parent = padre.transform;
                                voxels.Add(go.GetComponent<MeshFilter>());
                                count++;
                            }
                            j++;
                        }
                    }   

                    l[k].UnlockBits(bitmapData);
                }

                TimeSpan ts2 = st.Elapsed;

                // Format and display the TimeSpan value.
                string elapsedTime2 = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                    ts2.Hours, ts2.Minutes, ts2.Seconds,
                    ts2.Milliseconds / 10);
                print(elapsedTime2);

                int sets = Mathf.FloorToInt(count / chunkSize);
                holders = new GameObject[sets];
                for (int i = 0; i < sets; i++)
                {
                    holders[i] = Instantiate(holder, Vector3.zero, Quaternion.identity);
                    holders[i].name = c + "-Holder" + i;
                    CombineInstance[] combine = new CombineInstance[chunkSize];
                    for (int j = 0; j < chunkSize; j++)
                    {
                        if (j > count)
                            break;
                        combine[j].mesh = voxels[chunkSize * i + j].sharedMesh;
                        combine[j].transform = voxels[chunkSize * i + j].transform.localToWorldMatrix;
                    }
                    holders[i].GetComponent<MeshFilter>().mesh.CombineMeshes(combine);
                    holders[i].transform.parent = stacks[m].transform;
                    holders[i].GetComponent<Renderer>().material = channelMats[c];
                }
                voxels = null;
                Destroy(padre);
                System.GC.Collect();

                print("channel " + c + " processed - stack " + m);
                yield return null;
            }
            st.Stop();
            TimeSpan ts = st.Elapsed;

            // Format and display the TimeSpan value.
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                ts.Hours, ts.Minutes, ts.Seconds,
                ts.Milliseconds / 10);
            print(elapsedTime);

            stacks[m].SetActive(false);
        }

        set.transform.position = new Vector3(-.351f, .57f, -.39f);
        set.transform.localScale = .027567f * Vector3.one;
    }


    private IEnumerator LowLevelParsingRoutine3()
    {
        Stopwatch st = new Stopwatch();
        
        sequence = new byte[1][][][][]; // will be tiffiles;
        
        //for (int m = 0; m < tifFiles[0].Length; m++)
        for (int m = 0; m < 1; m++)
        {
            st.Start();
            sequence[m] = new byte[numChannels][][][];
            for (int c = 0; c < numChannels; c++)
            {
                int count = 0;
                string file = tifFiles[c][m];

                Tiffer t = new Tiffer();

                List<Bitmap> l = t.GetAllPagesAsBitmaps(file);
                sequence[m][c] = new byte[l.Count][][];

                for (int k = 0; k < l.Count; k++)
                {
                    BitmapData bitmapData = l[k].LockBits(new Rectangle(0, 0, l[k].Width, l[k].Height), ImageLockMode.ReadWrite, l[k].PixelFormat);

                    int bytesPerPixel = Bitmap.GetPixelFormatSize(l[k].PixelFormat) / 8;
                    int byteCount = bitmapData.Stride * l[k].Height;
                    byte[] pixels = new byte[byteCount];
                    IntPtr ptrFirstPixel = bitmapData.Scan0;
                    Marshal.Copy(ptrFirstPixel, pixels, 0, pixels.Length);
                    int heightInPixels = bitmapData.Height;
                    int widthInBytes = bitmapData.Width * bytesPerPixel;

                    wi = bitmapData.Width;
                    he = bitmapData.Height;

                    sequence[m][c][k] = new byte[heightInPixels][];

                    for (int y = 0; y < heightInPixels; y++)
                    {
                        int currentLine = y * bitmapData.Stride;
                        int j = 0;
                        sequence[m][c][k][y] = new byte[bitmapData.Width];
                        for (int x = 0; x < widthInBytes; x = x + bytesPerPixel)
                        {
                            if (pixels[currentLine + x] > bThreshold || pixels[currentLine + x + 1] > bThreshold || pixels[currentLine + x + 2] > bThreshold)
                            {
                                sequence[m][c][k][y][j] = (byte)(c + 1);
                            }
                            j++;
                        }
                    }

                    l[k].UnlockBits(bitmapData);
                }
            }
        }
        yield return null;

        TimeSpan ts = st.Elapsed;

        // Format and display the TimeSpan value.
        string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
            ts.Hours, ts.Minutes, ts.Seconds,
            ts.Milliseconds / 10);
        print(elapsedTime);


        //for (int m = 0; m < tifFiles[0].Length; m++)
        for (int m = 0; m < sequence.Length; m++)
        {
            for (int c = 0; c < sequence[m].Length; c++)
            {
                int count = 0;

                List<MeshFilter> voxels = new List<MeshFilter>();
                GameObject[] holders;
                GameObject padre = new GameObject();

                for (int k = 0; k < sequence[m][c].Length; k++)
                {
                    for (int y = 0; y < sequence[m][c][k].Length; y++)
                    {
                        for (int x = 0; x < sequence[m][c][k][y].Length; x++)
                        {
                            if (k == 0 || k == sequence[m][c].Length - 1 
                                || y == 0 || y == sequence[m][c][k].Length - 1
                                || x == 0 || x == sequence[m][c][k][y].Length-1) // edge, render all
                            {
                                if (sequence[m][c][k][y][x] == c + 1)
                                {
                                    GameObject go = (GameObject)Instantiate(voxel, new Vector3(1f * y / 10f, (k * .1f), 1f * x / 10f), Quaternion.identity);
                                    go.transform.parent = padre.transform;
                                    voxels.Add(go.GetComponent<MeshFilter>());
                                    count++;
                                }
                            }
                            else
                            {
                                if( CheckNeighbors(m,c,k,y,x))
                                {
                                    GameObject go = (GameObject)Instantiate(voxel, new Vector3(1f * y / 10f, (k * .1f), 1f * x / 10f), Quaternion.identity);
                                    go.transform.parent = padre.transform;
                                    voxels.Add(go.GetComponent<MeshFilter>());
                                    count++;
                                }
                            }
                        }
                    }
                }

                TimeSpan ts2 = st.Elapsed;

                // Format and display the TimeSpan value.
                string elapsedTime2 = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                    ts2.Hours, ts2.Minutes, ts2.Seconds,
                    ts2.Milliseconds / 10);
                print(elapsedTime2);

                int sets = Mathf.FloorToInt(count / chunkSize);
                holders = new GameObject[sets];
                for (int i = 0; i < sets; i++)
                {
                    holders[i] = Instantiate(holder, Vector3.zero, Quaternion.identity);
                    holders[i].name = c + "-Holder" + i;
                    CombineInstance[] combine = new CombineInstance[chunkSize];
                    for (int j = 0; j < chunkSize; j++)
                    {
                        if (j > count)
                            break;
                        combine[j].mesh = voxels[chunkSize * i + j].sharedMesh;
                        combine[j].transform = voxels[chunkSize * i + j].transform.localToWorldMatrix;
                    }
                    holders[i].GetComponent<MeshFilter>().mesh.CombineMeshes(combine);
                    holders[i].transform.parent = stacks[m].transform;
                    holders[i].GetComponent<Renderer>().material = channelMats[c];
                }
                voxels = null;
                Destroy(padre);
                System.GC.Collect();

                print("channel " + c + " processed - stack " + m);
                yield return null;
            }
            st.Stop();
            ts = st.Elapsed;

            // Format and display the TimeSpan value.
            elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                ts.Hours, ts.Minutes, ts.Seconds,
                ts.Milliseconds / 10);
            print(elapsedTime);

            stacks[m].SetActive(false);
        }

        set.transform.position = new Vector3(-.351f, .57f, -.39f);
        set.transform.localScale = .027567f * Vector3.one;
    }

    private bool CheckNeighbors(int m, int c, int k, int y, int x)
    {
        return (
            sequence[m][c][k][y - 1][x] != 0
            || sequence[m][c][k][y + 1][x] != 0
            || sequence[m][c][k][y][x - 1] != 0
            || sequence[m][c][k][y][x + 1] != 0
            || sequence[m][c][k][y-1][x - 1] != 0
            || sequence[m][c][k][y-1][x + 1] != 0
            || sequence[m][c][k][y+1][x - 1] != 0
            || sequence[m][c][k][y+1][x + 1] != 0

            || sequence[m][c][k-1][y + 1][x] != 0
            || sequence[m][c][k-1][y][x - 1] != 0
            || sequence[m][c][k-1][y][x + 1] != 0
            || sequence[m][c][k-1][y - 1][x - 1] != 0
            || sequence[m][c][k-1][y - 1][x + 1] != 0
            || sequence[m][c][k-1][y + 1][x - 1] != 0
            || sequence[m][c][k-1][y + 1][x + 1] != 0

            || sequence[m][c][k+1][y + 1][x] != 0
            || sequence[m][c][k+1][y][x - 1] != 0
            || sequence[m][c][k+1][y][x + 1] != 0
            || sequence[m][c][k+1][y - 1][x - 1] != 0
            || sequence[m][c][k+1][y - 1][x + 1] != 0
            || sequence[m][c][k+1][y + 1][x - 1] != 0
            || sequence[m][c][k+1][y + 1][x + 1] != 0
            );
    }

    private string[] GetFiles(int channel)
    {
        // BMDC_SL8_OT1_MBCD_CD45A647_TCRA488_18_ch0_stack0000_488nm_0000000msec_0001168547msecAbs_decon.tif
        string name1 = "BMDC_SL8_OT1_MBCD_CD45A647_TCRA488_18_ch";

        return Directory.GetFiles(tifFolder, name1 + channel + "*");
    }

    static unsafe void DetectColorWithUnsafe(Bitmap image, byte searchedR, byte searchedG, int searchedB, int tolerance)
    {
        BitmapData imageData = image.LockBits(new Rectangle(0, 0, image.Width, image.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
        int bytesPerPixel = 3;

        byte* scan0 = (byte*)imageData.Scan0.ToPointer();
        int stride = imageData.Stride;

        byte unmatchingValue = 0;
        byte matchingValue = 255;
        int toleranceSquared = tolerance * tolerance;

        for (int y = 0; y < imageData.Height; y++)
        {
            byte* row = scan0 + (y * stride);

            for (int x = 0; x < imageData.Width; x++)
            {
                // Watch out for actual order (BGR)!
                int bIndex = x * bytesPerPixel;
                int gIndex = bIndex + 1;
                int rIndex = bIndex + 2;

                byte pixelR = row[rIndex];
                byte pixelG = row[gIndex];
                byte pixelB = row[bIndex];

                int diffR = pixelR - searchedR;
                int diffG = pixelG - searchedG;
                int diffB = pixelB - searchedB;

                int distance = diffR * diffR + diffG * diffG + diffB * diffB;

                row[rIndex] = row[bIndex] = row[gIndex] = distance >
                  toleranceSquared ? unmatchingValue : matchingValue;
            }
        }

        image.UnlockBits(imageData);
    }

    // mirar https://stackoverflow.com/questions/6550933/what-should-i-do-to-use-taskt-in-net-2-0
    /*
    static unsafe void DetectColorWithUnsafeParallel(Bitmap image,
    byte searchedR, byte searchedG, int searchedB, int tolerance)
    {
        BitmapData imageData = image.LockBits(new Rectangle(0, 0, image.Width,
          image.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
        int bytesPerPixel = 3;

        byte* scan0 = (byte*)imageData.Scan0.ToPointer();
        int stride = imageData.Stride;

        byte unmatchingValue = 0;
        byte matchingValue = 255;
        int toleranceSquared = tolerance * tolerance;

        Task[] tasks = new Task[4];
        for (int i = 0; i < tasks.Length; i++)
        {
            int ii = i;
            tasks[i] = Task.Factory.StartNew(() =>
            {
                int minY = ii < 2 ? 0 : imageData.Height / 2;
                int maxY = ii < 2 ? imageData.Height / 2 : imageData.Height;

                int minX = ii % 2 == 0 ? 0 : imageData.Width / 2;
                int maxX = ii % 2 == 0 ? imageData.Width / 2 : imageData.Width;

                for (int y = minY; y < maxY; y++)
                {
                    byte* row = scan0 + (y * stride);

                    for (int x = minX; x < maxX; x++)
                    {
                        int bIndex = x * bytesPerPixel;
                        int gIndex = bIndex + 1;
                        int rIndex = bIndex + 2;

                        byte pixelR = row[rIndex];
                        byte pixelG = row[gIndex];
                        byte pixelB = row[bIndex];

                        int diffR = pixelR - searchedR;
                        int diffG = pixelG - searchedG;
                        int diffB = pixelB - searchedB;

                        int distance = diffR * diffR + diffG * diffG + diffB * diffB;

                        row[rIndex] = row[bIndex] = row[gIndex] = distance >
                            toleranceSquared ? unmatchingValue : matchingValue;
                    }
                }
            });
        }

        Task.WaitAll(tasks);

        image.UnlockBits(imageData);
    }
    */

    static void DetectColorWithMarshal(Bitmap image,
  byte searchedR, byte searchedG, int searchedB, int tolerance)
    {
        BitmapData imageData = image.LockBits(new Rectangle(0, 0, image.Width,
          image.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

        byte[] imageBytes = new byte[Math.Abs(imageData.Stride) * image.Height];
        IntPtr scan0 = imageData.Scan0;

        Marshal.Copy(scan0, imageBytes, 0, imageBytes.Length);

        byte unmatchingValue = 0;
        byte matchingValue = 255;
        int toleranceSquared = tolerance * tolerance;

        for (int i = 0; i < imageBytes.Length; i += 3)
        {
            byte pixelB = imageBytes[i];
            byte pixelR = imageBytes[i + 2];
            byte pixelG = imageBytes[i + 1];

            int diffR = pixelR - searchedR;
            int diffG = pixelG - searchedG;
            int diffB = pixelB - searchedB;

            int distance = diffR * diffR + diffG * diffG + diffB * diffB;

            imageBytes[i] = imageBytes[i + 1] = imageBytes[i + 2] = distance >
              toleranceSquared ? unmatchingValue : matchingValue;
        }

        Marshal.Copy(imageBytes, 0, scan0, imageBytes.Length);

        image.UnlockBits(imageData);
    }
}